<?php
////////////////////////////////////////////////////////////////////////////////
//   Copyright (C) ReloadCMS Development Team                                 //
//   http://reloadcms.com                                                     //
//   This product released under GNU General Public License v2                //
////////////////////////////////////////////////////////////////////////////////

$lang['def']['Add new urls to'] = 'Добавьте новые адреса в';
$lang['def']['Changefreq'] = 'Частота изменения';
$lang['def']['Configuration sitemap.xml'] = 'Настройки sitemap.xml';
$lang['def']['Enable'] = 'Включить';
$lang['def']['Example'] = 'Пример';
$lang['def']['Execution time: '] = 'Время исполнения: ';
$lang['def']['Look at'] = 'Посмотреть на';
$lang['def']['Memory peak usage: '] = 'Использовано памяти: ';
$lang['def']['Pack file to'] = 'Создать архив';
$lang['def']['Priority'] = 'Приоритет';
$lang['def']['Read more about Google Sitemap'] = 'Подробнее о SiteMap от Google';
$lang['def']['Register here'] = 'Зарегистрироваться можно здесь';
$lang['def']['Send to search engines'] = 'Послать поисковым машинам';
$lang['def']['Sitemap will be created for all news (except hidden).'] = 'Файл будет создан для всех новостей (кроме скрытых).';
$lang['def']['added successfully'] = 'успешно добавлен';
$lang['def']['updated successfully'] = 'успешно обновлен';
?>